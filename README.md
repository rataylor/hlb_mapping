# HLB_mapping

This is code created by Sadie J. Ryan for the paper "Predicting the fundamental thermal niche of crop pests and diseases in a changing world: a case study on citrus greening"
by Rachel A. Taylor, Sadie J. Ryan, Catherine A. Lippi, David G. Hall, Hossein A. Narouei-Khandan, Jason R. Rohr, and Leah R. Johnson

This code is used as part of the climate mapping of transmission of citrus greening at a pixel level around the world.

Please feel free to download and use the code but please do not make changes to this repository as it is representing code used in a specific paper.

For any queries about the code contact the repository owner, username rataylor